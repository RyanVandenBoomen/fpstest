﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour {

	Vector3 direction;
	[Header("Speed:")]
	public float accel;
	public float deccel;
	public float fSpeed;
	float fSpeedCur;
	public float sSpeed;
	float sSpeedCur;
	public float bSpeed;
	float bSpeedCur;
	[Header("Controls:")]
	public KeyCode forward;
	public KeyCode left;
	public KeyCode right;
	public KeyCode back;

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		Debug.Log (transform.rotation.eulerAngles.y + " Rotate");
		Debug.Log("Cos: " + Mathf.Cos(Mathf.Deg2Rad*transform.rotation.eulerAngles.y));
		Debug.Log ("Sin: " + Mathf.Sin (Mathf.Deg2Rad*transform.rotation.eulerAngles.y));

		direction = new Vector3 (Mathf.Sin (Mathf.Deg2Rad * transform.rotation.eulerAngles.y), 0, Mathf.Cos (Mathf.Deg2Rad * transform.rotation.eulerAngles.y));

		if (Input.GetKey (forward)) {
			fSpeedCur = Mathf.Lerp (fSpeedCur, fSpeed, accel*Time.deltaTime);
		}
		else {
			fSpeedCur = Mathf.Lerp (fSpeedCur, 0, deccel * Time.deltaTime);
		}
		if (Input.GetKey (back)) {
			bSpeedCur = Mathf.Lerp (bSpeedCur, bSpeed, accel*Time.deltaTime);
		}
		else {
			bSpeedCur = Mathf.Lerp(bSpeedCur, 0, deccel*Time.deltaTime);
		}
		if (Input.GetKey (left)) {
			sSpeedCur = Mathf.Lerp (sSpeedCur, -sSpeed, accel*Time.deltaTime);
		}
		else if (Input.GetKey (right)) {
			sSpeedCur = Mathf.Lerp (sSpeedCur, sSpeed, accel*Time.deltaTime);
		}
		else 
		{
			sSpeedCur = Mathf.Lerp(sSpeedCur, 0, deccel*Time.deltaTime);
		}
		if (fSpeedCur != 0 && bSpeedCur != 0) 
		{
			fSpeedCur = 0;
			bSpeedCur = 0;
		}
		if (Input.GetKey (left) && Input.GetKey (right)) 
		{
			sSpeedCur = 0;
		}
		this.transform.position += direction * fSpeedCur*Time.deltaTime;
		this.transform.position += -direction * bSpeedCur*Time.deltaTime;
		this.transform.position += new Vector3(direction.z, 0, -direction.x) * sSpeedCur*Time.deltaTime;
	}
}
