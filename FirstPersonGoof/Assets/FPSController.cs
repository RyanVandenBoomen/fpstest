﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FPSController : MonoBehaviour {
	Vector2 mouseLook;
	Vector2 smoothVector;
	GameObject player;
	[Header("Control Options")]
	public string xAxis;
	public string yAxis;
	public bool invert;
	public float sensitivity;
	public float smoothing;
	[Header("Max & Min Angles")]
	public float lowAngle;
	public float highAngle;
	public float angleSlowdown;


	// Use this for initialization
	void Start () {
		player = this.transform.parent.gameObject;
	}
	
	// Update is called once per frame
	void Update () {
		Vector2 input = new Vector2 (Input.GetAxisRaw (xAxis), Input.GetAxisRaw (yAxis));

		input = Vector2.Scale(input, new Vector2(sensitivity*smoothing, sensitivity*smoothing));
		smoothVector.x = Mathf.Lerp (smoothVector.x, input.x, 1f / smoothing);
		smoothVector.y = Mathf.Lerp (smoothVector.y, input.y, 1f / smoothing);

		mouseLook += smoothVector;



		if (mouseLook.y < lowAngle) 
		{
			mouseLook.y = Mathf.Lerp (mouseLook.y, lowAngle, angleSlowdown);
		}
		if (mouseLook.y > highAngle) 
		{
			mouseLook.y = Mathf.Lerp (mouseLook.y, highAngle, angleSlowdown);
		}

		transform.localRotation = Quaternion.AngleAxis (Mathf.Sign(System.Convert.ToSingle(invert)-0.5f)*mouseLook.y, Vector3.right);
		player.transform.localRotation = Quaternion.AngleAxis (mouseLook.x, player.transform.up);
	}
}
